﻿winrm delete winrm/config/listener?Address=*+Transport=HTTPS;

$cert = New-SelfSignedCertificate -DnsName winrm-cert -CertStoreLocation Cert:\LocalMachine\My
$thumb = $cert.Thumbprint

Enable-PSRemoting -Force -Verbose
Set-Item WSMan:\localhost\Client\TrustedHosts * -Force -Verbose
New-Item -Path WSMan:\localhost\Listener -Transport HTTPS -Address * -CertificateThumbPrint $thumb -Force
Restart-Service WinRM -Verbose
New-NetFirewallRule -DisplayName "Windows Remote Management (HTTPS-In)" -Name "WinRMHTTPSIn" -Profile Any -LocalPort 5986 -Protocol TCP